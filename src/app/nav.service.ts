import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { MoviePage } from './movie/moviePage';

@Injectable()
export class NavService {
  constructor(private http: HttpClient) {}

  
  handleError(err: HttpErrorResponse) {
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      errorMessage = 'Erreur produit' + err.error.message;
    } else {
      errorMessage = 'Erreur système';
    }
    return throwError(errorMessage);
  }
}
