import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MovieComponent } from './movie/movie.component';

const routes: Routes = [
  {path:'films/filtre/:filtre/:page', component: MovieComponent},
  {path:'', redirectTo:'films/filtre/popular/1',pathMatch:'full'},
  {path:'films/genre/:genreName/:genreId/:page', component: MovieComponent},
  {path:'films/search/:queryKey/:page', component: MovieComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
