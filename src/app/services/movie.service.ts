import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { MoviePage } from '../movie/moviePage';
import { Observable, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

@Injectable()
export class MovieService {
  constructor(private http: HttpClient) {}

  getPath(filtre: string, page: number) {
    let path =
      'https://api.themoviedb.org/3/movie/' +
      filtre +
      '?api_key=eebda35be2f8cab95effed35bfed00de&language&language=fr-FR&page=' +
      page;

    return path;
  }

  getPathPourGenre(genreId: number, page: number) {
    let path =
      'https://api.themoviedb.org/3/discover/movie?api_key=eebda35be2f8cab95effed35bfed00de&language=fr-FR&sort_by=popularity.desc&include_adult=false&include_video=false&page=' +
      page +
      '&with_genres=' +
      genreId;

    return path;
  }
  getPathSearch(queryKey: string, page: number) {
    let path =
      'https://api.themoviedb.org/3/search/movie?api_key=eebda35be2f8cab95effed35bfed00de&language=fr-FR&query=' +
      queryKey +
      '&page=' +
      page +
      '&include_adult=false';

    return path;
  }

  getMovies(filtre, page): Observable<MoviePage> {
    return this.http.get<MoviePage>(this.getPath(filtre, page)).pipe(
      tap((data) => console.log(JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  getMoviesParGenre(genreId, page): Observable<MoviePage> {
    return this.http.get<MoviePage>(this.getPathPourGenre(genreId, page)).pipe(
      tap((data) => console.log(JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  getMoviesSearchKey(queryKey, page): Observable<MoviePage> {
    return this.http.get<MoviePage>(this.getPathSearch(queryKey, page)).pipe(
      tap((data) => console.log(JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  handleError(err: HttpErrorResponse) {
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      errorMessage = 'Erreur produit' + err.error.message;
    } else {
      errorMessage = 'Erreur système';
    }
    return throwError(errorMessage);
  }
}
