import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { tap,catchError } from 'rxjs/operators';
import { ListGenres } from '../category/listGenres';

@Injectable()
export class CategoryService {

  constructor(private http: HttpClient) {}
  path: string =
    'https://api.themoviedb.org/3/genre/movie/list?api_key=eebda35be2f8cab95effed35bfed00de&language=fr-FR';
  
  getCategory(): Observable<ListGenres> {
    return this.http.get<ListGenres>(this.path).pipe(
      tap(data=>console.log(JSON.stringify(data))),
      catchError(this.handleError)
    );
  }
  handleError(err: HttpErrorResponse) {
    let errorMessage = '';
    if(err.error instanceof ErrorEvent){
      errorMessage ='Erreur produit' + err.error.message; 
    }else{
      errorMessage = 'Erreur système'
    }
    return throwError(errorMessage) ;
    
  }
}

