import { Movie } from './movie';

export class MoviePage {
    total_pages: number;
    results: Movie [];
    total_results: number;
    page: number;
    
  }