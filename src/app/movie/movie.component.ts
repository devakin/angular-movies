import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MovieService } from '../services/movie.service';

import { Movie } from './movie';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css'],
  providers: [MovieService],
})
export class MovieComponent implements OnInit {
  constructor(
    private movieService: MovieService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}
  title: string = 'Populaires';
  movies: Movie[];
  titlePage: String;

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      if (this.router.url.indexOf('films/genre/') > -1) {
        this.titlePage = params.genreName;
        this.movieService
          .getMoviesParGenre(params.genreId, params.page)
          .subscribe((data) => {
            this.movies = data.results;
          });
      } else if (this.router.url.indexOf('films/filtre/') > -1) {
        if (params.filtre === 'popular') this.titlePage = 'Populaires';
        if (params.filtre === 'now_playing') this.titlePage = 'Du moment';
        if (params.filtre === 'upcoming') this.titlePage = 'À venir';
        if (params.filtre === 'top_rated') this.titlePage = 'Les mieux notés';
        this.movieService
          .getMovies(params.filtre, params.page)
          .subscribe((data) => {
            this.movies = data.results;
          });
      } else if (this.router.url.indexOf('films/search/') > -1) {
        this.titlePage = 'pour ' + '"' + params.queryKey + '"';
        this.movieService
          .getMoviesSearchKey(params.queryKey, params.page)
          .subscribe((data) => {
            this.movies = data.results;
          });
      }
    });
  }
}
