export class Movie {
  popularity: number;
  video: boolean;
  genre_ids: number[];
  overview: string;
  original_language: string;
  original_title: string;
  poster_path: string;
  adult: boolean;
  backdrop_path: string;
  vote_average: number;
  title: string;
  id: number;
  vote_count: number;
  release_date: string;
}
