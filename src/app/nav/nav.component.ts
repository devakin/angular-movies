import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css'],
})
export class NavComponent implements OnInit {
  constructor(private router: Router) {}

  searchKey: string;
  ngOnInit(): void {}
  moviesSearchKey(searchKey, page = 1): void {
    let routerLink = 'films/search/' + searchKey + '/' + page;
    this.router.navigateByUrl(routerLink);
  }
}
